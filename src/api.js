import * as model from "./model.js"

import OError from "@overleaf/o-error"
import moment from "moment"
import qs from "qs"

// Error codes must match those defined in API
export const errorMessages = {
  BOPI_NOT_FOUND: ({ bopiId }) => `Le BOPI "${bopiId}" n'a pas été trouvé`,
  BOPI_ARCHIVED: ({ bopiId }) => `Le BOPI "${bopiId}" est archivé, la génération PDF n'est plus possible`,
  BOPI_ALREADY_RUNNING: ({ bopiId }) => `Le BOPI "${bopiId}" est déjà en cours de génération, la génération PDF n'est plus possible`,
}

export class ApiError extends OError {
  constructor(options) {
    super({ message: "Erreur liée au serveur", ...options })
  }
}

export class Api {
  constructor(config, { fetch } = {}) {
    this.config = config
    this.fetch = fetch
  }

  async bopiList(params) {
    // The list endpoint does not return any error code.
    const urlPath = `/bopi${qs.stringify(params, { addQueryPrefix: true })}`
    const responseData = await this.callEndpoint(urlPath)
    const { result } = responseData
    return { ...result, items: result.items.map(model.decodeBopi) }
  }

  async bopiShow(bopiId) {
    // The list endpoint does not return any error code.
    const urlPath = `/bopi/${bopiId}`
    const responseData = await this.callEndpoint(urlPath)

    if (responseData.error) {
      const { code } = responseData.error
      const message = code === "BOPI_NOT_FOUND" ? errorMessages[code]({ bopiId })
        : code
      const info = { urlPath, bopiId, responseData }
      throw new ApiError({ message, info }).withCause(responseData)
    }

    const { result } = responseData

    if (result.published_at) {
      result.published_at = moment(result.published_at)
    }

    return model.decodeBopi(result)
  }

  async bopiRun(bopiId, body) {
    const urlPath = `/bopi/${bopiId}/run`
    const responseData = await this.callEndpoint(urlPath, { method: "POST", body })

    if (responseData.error) {
      const { code } = responseData.error
      const message = code === "BOPI_NOT_FOUND" ? errorMessages[code]({ bopiId })
        : code === "BOPI_ARCHIVED" ? errorMessages[code]({ bopiId })
          : code === "BOPI_ALREADY_RUNNING" ? errorMessages[code]({ bopiId })
            : code
      const info = { urlPath, bopiId, body, responseData }
      throw new ApiError({ message, info }).withCause(responseData)
    }

    return responseData.result
  }

  async callEndpoint(urlPath, fetchOptions = {}) {
    const { method, body } = fetchOptions
    const url = this.config.apiUrl + urlPath
    const info = { url, urlPath, fetchOptions }

    const fetch = this.fetch || window.fetch

    let response
    try {
      response = await fetch(url, {
        method,
        mode: "cors",
        headers: { "Content-type": "application/json" },
        body: body ? JSON.stringify(body) : null
      })
      if (!response.ok) {
        throw new ApiError({ message: response.statusText || response.status }).withCause(response)
      }
    } catch (error) {
      throw new ApiError({ message: "Impossible de contacter le serveur", info }).withCause(error)
    }

    let responseData
    try {
      responseData = await response.json()
    } catch (error) {
      throw new ApiError({ message: "Impossible de décoder la réponse du serveur", info }).withCause(error)
    }

    return responseData
  }
}
