import "moment/locale/fr"

import * as sapper from "@sapper/app"

import moment from "moment"

moment.locale("fr")

sapper.start({
	target: document.querySelector("#sapper")
})