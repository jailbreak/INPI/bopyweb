function withoutTrailingSlash(s) {
  return s ? s.replace(/\/$/, "") : null
}

export function buildConfigFromEnv() {
  // Builds a config object that is intended to be run on server side.
  // Client side will receive config via Sapper session store.

  const apiUrl = withoutTrailingSlash(process.env.API_URL)
  if (!apiUrl) {
    throw Error("Undefined environment variable: API_URL")
  }

  const basePath = withoutTrailingSlash(process.env.BASE_PATH) || "/"

  const sectionCodes = process.env.SECTION_CODES
  if (!sectionCodes) {
    throw Error("Undefined environment variable: SECTION_CODES")
  }
  const sectionList = sectionCodes.split("|")

  const allSectionsCode = process.env.ALL_SECTIONS_CODE
  if (!allSectionsCode) {
    throw Error("Undefined environment variable: ALL_SECTIONS_CODE")
  }

  const flowerUrl = process.env.FLOWER_URL ? withoutTrailingSlash(process.env.FLOWER_URL) : null

  return { apiUrl, basePath, sectionList, allSectionsCode, flowerUrl }
}
