import "moment/locale/fr"
import "./styles/custom.scss"

import * as sapper from "@sapper/server"

import { buildConfigFromEnv } from "./config.js"
import compression from "compression"
import moment from "moment"
import polka from "polka"
import sirv from "sirv"

moment.locale("fr")

const config = buildConfigFromEnv()

const { PORT, NODE_ENV } = process.env
const dev = NODE_ENV === "development"

// console.log({ config })

polka() // You can also use Express
  .use(
    config.basePath,
    compression({ threshold: 0 }),
    sirv("static", { dev }),
    sapper.middleware({
      session: (/* req, res */) => ({ config })
    })
  )
  .listen(PORT, err => {
    if (err) console.log("error", err)
  })
