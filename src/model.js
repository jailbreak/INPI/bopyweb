import moment from "moment"

// Decoders

export function decodeBopi(data) {
  const bopi = { ...data }
  if (data.published_at) {
    bopi.published_at = moment(data.published_at)
  }
  if (data.input_files) {
    bopi.input_files = data.input_files.map(decodeInputFile)
  }
  if (data.tasks && data.tasks.latest) {
    data.tasks.latest = decodeTask(data.tasks.latest)
  }
  bopi._raw = data
  return bopi
}

export function decodeInputFile(data) {
  const inputFile = { ...data }
  if (data.updated_at) {
    inputFile.updated_at = moment(data.updated_at)
  }
  inputFile._raw = data
  return inputFile
}

export function decodeTask(data) {
  const task = { ...data }
  if (task.date_start) {
    task.date_start = moment(task.date_start)
  }
  if (task.date_done) {
    task.date_done = moment(task.date_done)
  }
  task._raw = data
  return task
}

// Functions

export function isGenerationInProgress(task) {
  return task && (task.state === "STARTED" || task.state === "PENDING")
}

export function taskDuration(task) {
  // If task is STARTED, it does not have any "date_done", so let's use "now" to measure execution time.
  const now = moment()
  const endDate = task.date_done || now
  const timeDeltaMilliseconds = endDate.diff(task.date_start)
  return moment.duration(timeDeltaMilliseconds)
}
