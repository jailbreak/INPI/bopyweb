# bopyweb-ui

L'interface Web de supervision de BOPY. Cette interface dialogue avec bopyweb-api
pour récupérer des informations sur le BOPI en cours et lancer la génération de document PDF.

bopyweb-ui repose sur [svelte](https://svelte.dev) et [sapper](https://sapper.svelte.dev/).

## Installation

```sh
npm install
```

## Configuration

Copier le fichier `.env.example` vers `.env` et adapter les valeurs. L'application fonctionne avec les valeurs par défaut.

## Démarrage du service

```sh
npm run dev
```

L'interface web est disponible à l'adresse http://localhost:3000
